import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components/native';

export default styled(LinearGradient).attrs({
  colors: ['#7d0606', '#d32f2f'],
})`
  flex: 1;
`;
