import styled from 'styled-components';
import {BaseButton} from 'react-native-gesture-handler';

export const Container = styled(BaseButton)`
  height: 46px;
  background: #ad0908;
  border-radius: 24px;

  align-items: center;
  justify-content: center;

  elevation: 3;
`;

export const Text = styled.Text`
  color: #fff;
  font-weight: bold;
  font-size: 16px;
`;
