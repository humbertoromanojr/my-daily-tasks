import React, {Component} from 'react';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Platform,
  Alert,
  StatusBar,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Touchable from 'react-native-platform-touchable';
import AsyncStorage from '@react-native-community/async-storage';

import moment from 'moment';
import 'moment/locale/pt-br';

import Background from '../components/Background';
import Footer from '../components/Footer';
import Task from '../components/Task';
import AddTask from '../screens/AddTask';

import commonStyles from '../commonStyles';
import todayImage from '../../assets/imgs/today.jpg';

const initialState = {
  showDoneTasks: true,
  showAddTask: false,
  visibleTasks: [],
  tasks: [],
};

export default class screens extends Component {
  state = {
    ...initialState,
  };

  componentDidMount = async () => {
    const tasksStateString = await AsyncStorage.getItem('tasksState');
    const state = JSON.parse(tasksStateString) || initialState;

    this.setState(state, this.filterTasks);
  };

  toggleFilter = () => {
    this.setState({showDoneTasks: !this.state.showDoneTasks}, this.filterTasks);
  };

  filterTasks = () => {
    let visibleTasks = null;
    if (this.state.showDoneTasks) {
      visibleTasks = [...this.state.tasks];
    } else {
      const pending = task => task.doneAt === null;
      visibleTasks = this.state.tasks.filter(pending);
    }

    this.setState({visibleTasks});
    AsyncStorage.setItem('tasksState', JSON.stringify(this.state));
  };

  toggleTask = taskId => {
    const tasks = [...this.state.tasks];
    tasks.forEach(task => {
      if (task.id === taskId) {
        task.doneAt = task.doneAt ? null : new Date();
      }
    });

    this.setState({tasks}, this.filterTasks);
  };

  addTask = newTask => {
    if (!newTask.desc || !newTask.desc.trim()) {
      Alert.alert('Dados inválidos:', 'Descrição não informada!');
      return;
    } else {
      const tasks = [...this.state.tasks];
      tasks.push({
        id: Math.random(),
        desc: newTask.desc,
        estimateAt: newTask.date,
        doneAt: null,
      });

      this.setState({tasks, showAddTask: false}, this.filterTasks);

      Alert.alert('Sucesso:', 'Task cadastrada');
    }
  };

  deleteTask = id => {
    const tasks = this.state.tasks.filter(task => task.id !== id);
    this.setState({tasks}, this.filterTasks);
  };

  render() {
    const today = moment()
      .locale('pt-br')
      .format('ddd, D [de] MMMM');

    return (
      <Background>
        <View style={styles.container}>
          <StatusBar backgroundColor="#7d0606" barStyle="light-content" />
          <AddTask
            isVisible={this.state.showAddTask}
            onCancel={() => this.setState({showAddTask: false})}
            onSave={this.addTask}
          />
          <ImageBackground source={todayImage} style={styles.backgroundImage}>
            <View style={styles.iconBar}>
              <TouchableOpacity onPress={this.toggleFilter}>
                <Icon
                  name={this.state.showDoneTasks ? 'eye' : 'eye-slash'}
                  size={30}
                  color={commonStyles.colors.secondary}
                />
              </TouchableOpacity>
            </View>
            <View style={styles.titleBar}>
              <Text style={styles.name}>My Daily Tasks</Text>
              <View style={styles.containerDisplay}>
                {/* <Text style={styles.title}>{this.props.title}</Text> */}
                <Text style={styles.title}>Hoje</Text>
                <Text style={styles.subTitle}>{today}</Text>
              </View>
            </View>
          </ImageBackground>
          <View style={styles.taskList}>
            <FlatList
              data={this.state.visibleTasks}
              keyExtractor={item => `${item.id}`}
              renderItem={({item}) => (
                <Task
                  {...item}
                  onToggleTask={this.toggleTask}
                  onDelete={this.deleteTask}
                />
              )}
            />
          </View>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.addButton}
            onPress={() => this.setState({showAddTask: true})}>
            <Icon name="plus" size={20} color="#fff" />
          </TouchableOpacity>
          {/* <Footer /> */}
        </View>
      </Background>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundImage: {
    flex: 3,
  },
  taskList: {
    flex: 7,
  },
  titleBar: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  name: {
    fontFamily: commonStyles.fontFamily,
    color: commonStyles.colors.secondary,
    fontSize: 30,
    paddingBottom: 45,
    paddingVertical: 20,
    paddingLeft: 10,
  },
  containerDisplay: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 90,
    marginHorizontal: 10,
  },
  title: {
    fontFamily: 'Roboto',
    color: commonStyles.colors.secondary,
    fontSize: 30,
  },
  subTitle: {
    fontFamily: 'Roboto',
    fontSize: 18,
    marginTop: 10,
    color: '#ddd',
  },
  iconBar: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginHorizontal: 10,
    marginTop: Platform.OS === 'ios' ? 50 : 10,
    zIndex: 999,
  },
  addButton: {
    position: 'absolute',
    right: 30,
    bottom: 30,
    width: 50,
    height: 50,
    borderWidth: 2,
    borderColor: '#fff',
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 12,
    backgroundColor: '#ad0908',
  },
});
